// This file is a part of Akel
// Authors : @kbz_8
// Created : 27/03/2023
// Updated : 07/07/2023

#include <Animation/animator_component.h>

namespace Ak
{
	AnimatorComponent::AnimatorComponent() : Component("__animator_component") {}

	void AnimatorComponent::onAttach()
	{

	}

	void AnimatorComponent::onFixedUpdate()
	{

	}

	void AnimatorComponent::onQuit()
	{

	}
}
