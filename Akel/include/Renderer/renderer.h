// This file is a part of Akel
// Authors : @kbz_8
// Created : 07/05/2021
// Updated : 09/07/2023

#ifndef __AK_RENDERER__
#define __AK_RENDERER__

#include "rendererComponent.h"
#include <Renderer/Core/render_core.h>
#include "scene_renderer.h"
#include "render_command.h"
#include "renderer_events.h"

#endif // __AK_RENDERER__
