// This file is a part of Akel
// Authors : @kbz_8
// Created : 19/04/2021
// Updated : 28/01/2023

#ifndef __AK_HARDWARE_INFO__
#define __AK_HARDWARE_INFO__

#include <Akpch.h>

namespace Ak::Core
{
    void printCPUinfo();
    void printGPUinfo();
}

#endif // __AK_HARDWARE_INFO__
