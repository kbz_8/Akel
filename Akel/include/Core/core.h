// This file is a part of Akel
// Authors : @kbz_8
// Created : 03/04/2021
// Updated : 01/07/2023

#ifndef __AK_CORE__
#define __AK_CORE__

#include <Core/log.h>
#include <Core/softwareInfo.h>
#include <Core/hardwareInfo.h>
#include <Core/cpu.h>
#include <Core/gpu.h>
#include <Core/application.h>
#include <Core/profile.h>
#include <Core/projectFile.h>
#include <Core/initialisation.h>
#include <Core/instance.h>
#include <Core/file_loader.h>
#include <Core/vfs.h>

#include <Core/Components/components.h>
#include <Core/Memory/memory.h>
#include <Core/Event/event.h>

#endif // __AK_CORE__
