// This file is a part of Akel
// Authors : @kbz_8
// Created : 16/02/2023
// Updated : 27/03/2023

#ifndef __AK_ATTRIBUTES__
#define __AK_ATTRIBUTES__

#include "model_attribute.h"
#include "name_attribute.h"
#include "script_attribute.h"
#include "transform_attribute.h"
#include "audio_attribute.h"

#endif
