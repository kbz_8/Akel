// This file is a part of Akel
// Authors : @kbz_8
// Created : 14/02/2023
// Updated : 14/02/2023

#ifndef __AK_NAME_ATTRIBUTE__
#define __AK_NAME_ATTRIBUTE__

#include <Akpch.h>

namespace Ak
{
	struct AK_API NameAttribute
	{
		std::string name;
	};
}

#endif
