// This file is a part of Akel
// Authors : @kbz_8
// Created : 03/06/2021
// Updated : 11/03/2023

#ifndef __AK_GRAPHICS__
#define __AK_GRAPHICS__

#include <Graphics/mesh.h>
#include <Graphics/vertex.h>
#include <Graphics/model.h>
#include <Graphics/model_factory.h>
#include <Graphics/material.h>
#include <Graphics/material_library.h>

#endif // __AK_GRAPHICS__

