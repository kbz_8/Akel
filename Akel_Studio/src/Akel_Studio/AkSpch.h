// This file is a part of Akel Studio
// Authors : @kbz_8
// Created : 07/06/2021
// Updated : 25/05/2023

#ifndef __AK_STUDIO_PCH__
#define __AK_STUDIO_PCH__

#if !defined(AK_DEBUG) && !defined(AK_RELEASE)
	#define AK_RELEASE
#endif

#include <Akpch.h>
#include <Akel.h>
//#include <ImGuizmo.h>

#endif // __AK_STUDIO_PCH__
